package by.testingtask.authorization;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Вспомогательный класс реализующий вывод полученных данных о залогировавшемся пользователе.
 */

public class UserInformationActivity extends Activity {

    private TextView textViewName;
    private TextView textViewEmail;
    private Button buttonLogout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_information_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        init();
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            /**
             * При нажатии кнопки приосходит удаление данных для автоматической
             * авторизации и закрытие активити.
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                SharedPreferences sPref;
                sPref = getSharedPreferences("LogInfo", MODE_PRIVATE);
                SharedPreferences.Editor ed = sPref.edit();
                ed.putString("login", "");
                ed.putString("password", "");
                ed.commit();
                finish();
            }
        });
    }

    /**
     * Инифиализирует компоненты на форме
     */
    private void init() {
        textViewName = findViewById(R.id.textViewName);
        textViewEmail = findViewById(R.id.textViewEmail);
        buttonLogout = findViewById(R.id.buttonLogout);
        textViewName.setText(getIntent().getStringExtra("name"));
        textViewEmail.setText(getIntent().getStringExtra("email"));
    }
}
