package by.testingtask.authorization;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Главный класс реализиющий логику авторизации.
 */
public class MainActivity extends Activity {

    /**
     * AsyncTask для отправки данных на сервер в новом потоке
     */
    private PostQuery mt;

    private Button buttonAuth;
    private EditText editTextLog;
    private EditText editTextPass;
    private TextView textViewLog;
    private TextView textViewPassword;
    private ProgressBar progressBar;
    private TextView textViewProgressBar;


    /**
     * Ссылка на php-файл принимающий данные для авторицаии
     * и возвращающий информацию о пользователе, либо сообщение об ошибке авторизации
     */
    private final String HOST = "http://10.0.3.2:6449/external_authorization.php";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.main_layout);

        init();

        buttonAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authorization(HOST, editTextLog.getText().toString(), editTextPass.getText().toString());
            }

        });

        authorization();

    }

    /**
     * Перегруженный метод authorization с параметрами. Производит попытку авторизации
     * используя переданные данные. В случае успешной авторизации, данные для авторизации
     * записываются в файл LogInfo.
     *
     * @param HOST - ссылка на сервер
     * @param login - логин
     * @param password - пароль
     */
    private void authorization(String HOST, String login, String password) {
        SharedPreferences sPref;
        sPref = getSharedPreferences("LogInfo", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("login", login);
        ed.putString("password", password);
        ed.commit();

        if (!login.equals("") && !password.equals("")) {
            mt = new PostQuery();
            mt.execute(HOST, login, password);
        }
    }

    /**
     * Перегруженный метод authorization без параметров. Обращается к серверу определённому
     * в константе HOST класса MainActivity, данные по логину и паролю берёт из файла LogInfo.
     */
    private void authorization() {
        SharedPreferences sPref;
        sPref = getSharedPreferences("LogInfo", MODE_PRIVATE);
        String login = sPref.getString("login", "");
        String password = sPref.getString("password", "");

        if (!login.equals("") && !password.equals("")) {
            mt = new PostQuery();
            mt.execute(HOST, login, password);
        }
    }

    /**
     * Метод logout удаляет данные для авторизации из файла LogInfo
     */
    private void logout() {
        SharedPreferences sPref;
        sPref = getSharedPreferences("LogInfo", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("login", "");
        ed.putString("password", "");
    }

    /**
     * Отображает на слое компоненты прогрессбара
     */
    private void showProgressBarViews() {
        progressBar.setVisibility(View.VISIBLE);
        textViewProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Прячет на слое компоненты прогрессбара
     */
    private void hideProgressBarViews() {
        progressBar.setVisibility(View.INVISIBLE);
        textViewProgressBar.setVisibility(View.INVISIBLE);
    }

    /**
     * Показывает на форме комоненты для авторизации
     */
    private void showAuthorizationViews() {
        buttonAuth.setVisibility(View.VISIBLE);
        editTextLog.setVisibility(View.VISIBLE);
        editTextPass.setVisibility(View.VISIBLE);
        textViewLog.setVisibility(View.VISIBLE);
        textViewPassword.setVisibility(View.VISIBLE);
    }

    /**
     * Прячет на форме компоненты для авторизации
     */
    private void hideAuthorizationViews() {
        buttonAuth.setVisibility(View.INVISIBLE);
        editTextLog.setVisibility(View.INVISIBLE);
        editTextPass.setVisibility(View.INVISIBLE);
        textViewLog.setVisibility(View.INVISIBLE);
        textViewPassword.setVisibility(View.INVISIBLE);
    }

    /**
     * Инициализирует компоненты на форме
     */
    private void init() {

        buttonAuth = findViewById(R.id.button_auth);
        editTextLog = findViewById(R.id.editText_log);
        editTextPass = findViewById(R.id.editText_pass);
        textViewLog = findViewById(R.id.textView_log);
        textViewPassword = findViewById(R.id.textView_pass);

        progressBar = findViewById(R.id.progressBar);
        textViewProgressBar = findViewById(R.id.textView_progressBar);

        progressBar.setVisibility(View.INVISIBLE);
        textViewProgressBar.setVisibility(View.INVISIBLE);

    }

    /**
     * Объект класса PostQuery принимает данные необходимые для авторизации и отвечает за
     * отправку POST-запроса на указанный сервер с передачей параметров: логина и пароля.
     * Так же за обработку прищедшего ответа.
     */
    private class PostQuery extends AsyncTask<String, View, String> {


        /**
         * Перед отправкой запроса метод отрисовывает прогрессбар на дисплее
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBarViews();
            hideAuthorizationViews();
        }


        /**
         * Метод описывающий POST-запрос.
         * @param params - массив содержащий данные с логином и паролем.
         * @return - возвращает строку содержащую ответ сервера.
         */
        @Override
        protected String doInBackground(String... params) {

            String request = "";


            try {
                

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost http = new HttpPost(params[0]);

                List nameValuePairs = new ArrayList(2);
                nameValuePairs.add(new BasicNameValuePair("login", params[1]));
                nameValuePairs.add(new BasicNameValuePair("password", params[2]));

                http.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                request = (String) httpclient.execute(http, new BasicResponseHandler());

                System.out.println(request);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return request;
        }

        /**
         * После получения ответа от сервера происходит его интерпретация.
         * Если получения строка содержит ошибочное сообщение или пуста, появляется
         * соответсвующая ошибка. В ином случае происходит вызов UserInformationActivity
         * с передачей данных с именем пользователя и его email'ом.
         *
         * @param request - ответ сервера.
         */
        @Override
        protected void onPostExecute(String request) {
            super.onPostExecute(request);

            hideProgressBarViews();
            showAuthorizationViews();

            if (request.equals("") || request.equals("Wrong login or password")) {
                String message = request.equals("") ? "No server connect" : request;
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                logout();
                return;
            }

            String[] data = request.split(",");

            Intent intent = new Intent(MainActivity.this, UserInformationActivity.class);
            intent.putExtra("email", data[0]);
            intent.putExtra("name", data[1]);

            startActivity(intent);
        }

    }
}

